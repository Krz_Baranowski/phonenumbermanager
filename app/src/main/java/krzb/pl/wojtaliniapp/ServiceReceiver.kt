package krzb.pl.wojtaliniapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.jetbrains.anko.doAsync
import java.io.IOException


class ServiceReceiver : BroadcastReceiver()
{
    val JSON = MediaType.parse("application/json; charset=utf-8")

    var client = OkHttpClient()

    override fun onReceive(context: Context, intent: Intent)
    {
        try
        {
            val state = intent.getStringExtra(TelephonyManager.EXTRA_STATE)
            val incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)
            if (state == TelephonyManager.EXTRA_STATE_RINGING)
            {
                Toast.makeText(context, "Ringing State Number is - $incomingNumber", Toast.LENGTH_SHORT).show()

                var result = post(AppHelper.hostName, incomingNumber)
            }

        } catch (e: Exception)
        {
            e.printStackTrace()
        }
    }

    @Throws(IOException::class)
    fun post(url: String, json: String): String?
    {
        val body = RequestBody.create(JSON, json)
        val request = Request.Builder()
                .url(url)
                .post(body)
                .build()
        client.newCall(request).execute()

        Log.w("Number", json)
        return ""
    }
}